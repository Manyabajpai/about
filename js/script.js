function show(){
    if(screen.width < 480){
        document.getElementById("plus").style.display = "none";
        document.getElementById("plus").style.opacity = "0";
        document.getElementById("cross").style.opacity = "1";
        document.getElementById("menumob").style.opacity = "1";
    }
    else{
        document.getElementById("plus").style.display = "none";
        document.getElementById("cross").style.opacity = "1";
        document.getElementById("menu").style.opacity = "1";
    }
    
}

function hide(){
    if(screen.width < 480){
        document.getElementById("plus").style.display = "none";
        document.getElementById("cross").style.opacity = "0";
        document.getElementById("plus").style.opacity = "1";
        document.getElementById("menumob").style.opacity = "0";
    }
    else{
        document.getElementById("plus").style.display = "block";
        document.getElementById("cross").style.opacity = "0"; 
        document.getElementById("menu").style.opacity = "0";
    }
    
}